﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <stdint.h>
#include <limits>



// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5;
// UnityEngine.Color[]
struct ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// System.String
struct String_t;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;



IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// PopcornMaker/<KillCube>d__13
struct U3CKillCubeU3Ed__13_t51629EE890C0A67E39524FB7180518C984688E2C  : public RuntimeObject
{
public:
	// System.Int32 PopcornMaker/<KillCube>d__13::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PopcornMaker/<KillCube>d__13::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.GameObject PopcornMaker/<KillCube>d__13::go
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___go_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CKillCubeU3Ed__13_t51629EE890C0A67E39524FB7180518C984688E2C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CKillCubeU3Ed__13_t51629EE890C0A67E39524FB7180518C984688E2C, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_go_2() { return static_cast<int32_t>(offsetof(U3CKillCubeU3Ed__13_t51629EE890C0A67E39524FB7180518C984688E2C, ___go_2)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_go_2() const { return ___go_2; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_go_2() { return &___go_2; }
	inline void set_go_2(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___go_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___go_2), (void*)value);
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// PopcornMaker
struct PopcornMaker_tA901AF1C66E33EEC164354FF805F105EC8550BE3  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Transform PopcornMaker::_spawnPoint
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ____spawnPoint_4;
	// UnityEngine.GameObject PopcornMaker::_cubePrefab
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ____cubePrefab_5;
	// System.Boolean PopcornMaker::_spawn
	bool ____spawn_6;
	// UnityEngine.Transform PopcornMaker::_BoxCover
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ____BoxCover_7;
	// System.Single PopcornMaker::_timer
	float ____timer_10;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> PopcornMaker::_cubes
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ____cubes_11;
	// UnityEngine.Color[] PopcornMaker::colors
	ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* ___colors_12;

public:
	inline static int32_t get_offset_of__spawnPoint_4() { return static_cast<int32_t>(offsetof(PopcornMaker_tA901AF1C66E33EEC164354FF805F105EC8550BE3, ____spawnPoint_4)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get__spawnPoint_4() const { return ____spawnPoint_4; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of__spawnPoint_4() { return &____spawnPoint_4; }
	inline void set__spawnPoint_4(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		____spawnPoint_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____spawnPoint_4), (void*)value);
	}

	inline static int32_t get_offset_of__cubePrefab_5() { return static_cast<int32_t>(offsetof(PopcornMaker_tA901AF1C66E33EEC164354FF805F105EC8550BE3, ____cubePrefab_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get__cubePrefab_5() const { return ____cubePrefab_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of__cubePrefab_5() { return &____cubePrefab_5; }
	inline void set__cubePrefab_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		____cubePrefab_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____cubePrefab_5), (void*)value);
	}

	inline static int32_t get_offset_of__spawn_6() { return static_cast<int32_t>(offsetof(PopcornMaker_tA901AF1C66E33EEC164354FF805F105EC8550BE3, ____spawn_6)); }
	inline bool get__spawn_6() const { return ____spawn_6; }
	inline bool* get_address_of__spawn_6() { return &____spawn_6; }
	inline void set__spawn_6(bool value)
	{
		____spawn_6 = value;
	}

	inline static int32_t get_offset_of__BoxCover_7() { return static_cast<int32_t>(offsetof(PopcornMaker_tA901AF1C66E33EEC164354FF805F105EC8550BE3, ____BoxCover_7)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get__BoxCover_7() const { return ____BoxCover_7; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of__BoxCover_7() { return &____BoxCover_7; }
	inline void set__BoxCover_7(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		____BoxCover_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____BoxCover_7), (void*)value);
	}

	inline static int32_t get_offset_of__timer_10() { return static_cast<int32_t>(offsetof(PopcornMaker_tA901AF1C66E33EEC164354FF805F105EC8550BE3, ____timer_10)); }
	inline float get__timer_10() const { return ____timer_10; }
	inline float* get_address_of__timer_10() { return &____timer_10; }
	inline void set__timer_10(float value)
	{
		____timer_10 = value;
	}

	inline static int32_t get_offset_of__cubes_11() { return static_cast<int32_t>(offsetof(PopcornMaker_tA901AF1C66E33EEC164354FF805F105EC8550BE3, ____cubes_11)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get__cubes_11() const { return ____cubes_11; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of__cubes_11() { return &____cubes_11; }
	inline void set__cubes_11(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		____cubes_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____cubes_11), (void*)value);
	}

	inline static int32_t get_offset_of_colors_12() { return static_cast<int32_t>(offsetof(PopcornMaker_tA901AF1C66E33EEC164354FF805F105EC8550BE3, ___colors_12)); }
	inline ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* get_colors_12() const { return ___colors_12; }
	inline ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834** get_address_of_colors_12() { return &___colors_12; }
	inline void set_colors_12(ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* value)
	{
		___colors_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___colors_12), (void*)value);
	}
};


// UIButton_ShowNext
struct UIButton_ShowNext_tBF8EB2E9E7E3443079BB68B854D83CC254CA0DCF  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject[] UIButton_ShowNext::GameObjectsList
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___GameObjectsList_4;
	// System.Int32 UIButton_ShowNext::shownGameObjectIndex
	int32_t ___shownGameObjectIndex_5;

public:
	inline static int32_t get_offset_of_GameObjectsList_4() { return static_cast<int32_t>(offsetof(UIButton_ShowNext_tBF8EB2E9E7E3443079BB68B854D83CC254CA0DCF, ___GameObjectsList_4)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_GameObjectsList_4() const { return ___GameObjectsList_4; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_GameObjectsList_4() { return &___GameObjectsList_4; }
	inline void set_GameObjectsList_4(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___GameObjectsList_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GameObjectsList_4), (void*)value);
	}

	inline static int32_t get_offset_of_shownGameObjectIndex_5() { return static_cast<int32_t>(offsetof(UIButton_ShowNext_tBF8EB2E9E7E3443079BB68B854D83CC254CA0DCF, ___shownGameObjectIndex_5)); }
	inline int32_t get_shownGameObjectIndex_5() const { return ___shownGameObjectIndex_5; }
	inline int32_t* get_address_of_shownGameObjectIndex_5() { return &___shownGameObjectIndex_5; }
	inline void set_shownGameObjectIndex_5(int32_t value)
	{
		___shownGameObjectIndex_5 = value;
	}
};


// rotateIt
struct rotateIt_t6099651DFE8ECA9072C0CE306AE5394D94D88D61  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3227[2] = 
{
	UIButton_ShowNext_tBF8EB2E9E7E3443079BB68B854D83CC254CA0DCF::get_offset_of_GameObjectsList_4(),
	UIButton_ShowNext_tBF8EB2E9E7E3443079BB68B854D83CC254CA0DCF::get_offset_of_shownGameObjectIndex_5(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3228[3] = 
{
	U3CKillCubeU3Ed__13_t51629EE890C0A67E39524FB7180518C984688E2C::get_offset_of_U3CU3E1__state_0(),
	U3CKillCubeU3Ed__13_t51629EE890C0A67E39524FB7180518C984688E2C::get_offset_of_U3CU3E2__current_1(),
	U3CKillCubeU3Ed__13_t51629EE890C0A67E39524FB7180518C984688E2C::get_offset_of_go_2(),
};
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable3229[9] = 
{
	PopcornMaker_tA901AF1C66E33EEC164354FF805F105EC8550BE3::get_offset_of__spawnPoint_4(),
	PopcornMaker_tA901AF1C66E33EEC164354FF805F105EC8550BE3::get_offset_of__cubePrefab_5(),
	PopcornMaker_tA901AF1C66E33EEC164354FF805F105EC8550BE3::get_offset_of__spawn_6(),
	PopcornMaker_tA901AF1C66E33EEC164354FF805F105EC8550BE3::get_offset_of__BoxCover_7(),
	0,
	0,
	PopcornMaker_tA901AF1C66E33EEC164354FF805F105EC8550BE3::get_offset_of__timer_10(),
	PopcornMaker_tA901AF1C66E33EEC164354FF805F105EC8550BE3::get_offset_of__cubes_11(),
	PopcornMaker_tA901AF1C66E33EEC164354FF805F105EC8550BE3::get_offset_of_colors_12(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
