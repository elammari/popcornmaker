﻿using UnityEngine;
using System.Collections.Generic;


    /// <summary>
    /// Spawns cube prefabs from a transform and removes them once they reach a maximum number
    /// </summary>
    public class PopcornMaker : MonoBehaviour
    {
        [SerializeField]
        private Transform _spawnPoint = null;

        [SerializeField]
        private GameObject _cubePrefab = null;

        [SerializeField]
        private bool _spawn = true;

    [SerializeField]
    private Transform _BoxCover;

    [SerializeField]
    private AudioSource audio;

    private const int MaxCubes = 200;//48;
        private const float SpawnTime = 0.25f;

        // State
        private float _timer = SpawnTime;
        private List<GameObject> _cubes = new List<GameObject>(32);

    Color[] colors = new Color[3];

    void Start()
    {
        colors[0] = Color.yellow;
        colors[1] = Color.black;
        colors[2] = Color.white;
       // colors[3] = new Color(255, 165, 0);
       // colors[4] = Color.yellow;
       // colors[5] = Color.magenta;
    }

    private void Update()
        {
            // Spawn cubes at a certain rate
            _timer -= Time.deltaTime;
            if (_timer <= 0f)
            {
                if (_spawn)
                {
                    _timer = SpawnTime;
                    SpawnCube();
                }

                // Remove cubes when there are too many
                if (_cubes.Count > MaxCubes || !_spawn)
                {
                // RemoveCube();
               _spawn = false;
                _BoxCover.gameObject.SetActive(false);
                }
            }

              //Debug.Log(_cubes.Count);
        }

        private void SpawnCube()
        {
            Quaternion rotation = Quaternion.Euler(Random.Range(-180f, 180f), Random.Range(-180f, 180f), Random.Range(-180f, 180f));
            float scale = Random.Range(0.1f, 0.6f);

            GameObject go = (GameObject)GameObject.Instantiate(_cubePrefab, _spawnPoint.position, rotation);
            audio.Play();

           Transform t = go.GetComponent<Transform>();
            go.GetComponent<Rigidbody>().AddExplosionForce(10f, _spawnPoint.position, 5f, 0f, ForceMode.Impulse);

           go.GetComponent<MeshRenderer>().material.color = colors[Random.Range(0, colors.Length)];

        //AddExplosionForce(float explosionForce, Vector3 explosionPosition, float explosionRadius, float upwardsModifier = 0.0F, ForceMode mode = ForceMode.Force);
        t.localScale = new Vector3(scale * 2f, scale*2f, scale * 2f);
            //t.SetParent(_spawnPoint);
            _cubes.Add(go);
        }

        private void RemoveCube()
        {
            if (_cubes.Count > 0)
            {
                // Remove the oldest cube
                GameObject go = _cubes[0];

                // Disabling the collider makes it fall through the floor - which is a neat way to hide its removal
               // go.GetComponent<Collider>().enabled = false;
                _cubes.RemoveAt(0);
                StartCoroutine("KillCube", go);
            }
        }

        private System.Collections.IEnumerator KillCube(GameObject go)
        {
            yield return new WaitForSeconds(1.5f);
            Destroy(go);
        }
    }
 