﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void FreeCam::Update()
extern void FreeCam_Update_m0E525803336780E25890CE2E36C59D00FF0174BE (void);
// 0x00000002 System.Void FreeCam::OnDisable()
extern void FreeCam_OnDisable_m383EA93DC78C8D2ABB533AD1A52728B1C8C0F583 (void);
// 0x00000003 System.Void FreeCam::StartLooking()
extern void FreeCam_StartLooking_mA80F357671A73F3337CD527CCEBDF84357F7798D (void);
// 0x00000004 System.Void FreeCam::StopLooking()
extern void FreeCam_StopLooking_mBCED5313D885DF0DAC563FEA56E8D80D62CFF0A4 (void);
// 0x00000005 System.Void FreeCam::.ctor()
extern void FreeCam__ctor_m718FB70858AD27D4118F107BADDEA789E6FA5AC7 (void);
// 0x00000006 System.Void FreeCamera::Awake()
extern void FreeCamera_Awake_m3B544F81CD5A7CC7B1110954697069EDCAF19050 (void);
// 0x00000007 System.Void FreeCamera::OnValidate()
extern void FreeCamera_OnValidate_m1C0B74AF3C24422FAF1ABB2164496634FF5A40A2 (void);
// 0x00000008 System.Void FreeCamera::CaptureInput()
extern void FreeCamera_CaptureInput_m2A98EA3929399EEBC609E4C7B793BBA6153A5DCC (void);
// 0x00000009 System.Void FreeCamera::ReleaseInput()
extern void FreeCamera_ReleaseInput_m3323C533F089391E8E97F5506213B0D37BD421A6 (void);
// 0x0000000A System.Void FreeCamera::OnApplicationFocus(System.Boolean)
extern void FreeCamera_OnApplicationFocus_m34A575C443ABB9F2ED9C351D9AA4F3D3B45723E2 (void);
// 0x0000000B System.Void FreeCamera::Update()
extern void FreeCamera_Update_m3B78A297B74E8DBB800B383700970C548B796281 (void);
// 0x0000000C System.Void FreeCamera::.ctor()
extern void FreeCamera__ctor_m5D39D27AEC0DBFCC7FDE8470605D6E0A7A1BA357 (void);
// 0x0000000D System.Void SuperCamera::Start()
extern void SuperCamera_Start_m8CFB1B49FDB2DA4118EA56A560E102BA5CEBF3B8 (void);
// 0x0000000E System.Void SuperCamera::Update()
extern void SuperCamera_Update_m11D2D6E9A26595BDE649F865FDB733AED6F3EA15 (void);
// 0x0000000F System.Void SuperCamera::.ctor()
extern void SuperCamera__ctor_m2D5DF21B9F75016763866ED241A0481B34D86863 (void);
// 0x00000010 System.Void UIButton_ShowNext::Start()
extern void UIButton_ShowNext_Start_m1C46EEA00D55364FBC3BD560FB3E8B3F3D11666D (void);
// 0x00000011 System.Void UIButton_ShowNext::SelectNextGameObject()
extern void UIButton_ShowNext_SelectNextGameObject_m06ACE4636D8DB6650B2DAED0D3335C3162F3280F (void);
// 0x00000012 System.Void UIButton_ShowNext::SelectPreviousGameObject()
extern void UIButton_ShowNext_SelectPreviousGameObject_mD21B08B5EEC5049635EEE7A85C9FC40CD8C0CE88 (void);
// 0x00000013 System.Void UIButton_ShowNext::SelectGameObject(System.Int32)
extern void UIButton_ShowNext_SelectGameObject_mC299122AECE4C47905FBF0D2179FBC4A84E7300B (void);
// 0x00000014 System.Void UIButton_ShowNext::.ctor()
extern void UIButton_ShowNext__ctor_mE73382473DB9B72F22C0375EDF19BFD2B3529C94 (void);
// 0x00000015 System.Void PopcornMaker::Start()
extern void PopcornMaker_Start_m8A37DAF3E3A04C6BB318212EFC6CD35F899579C7 (void);
// 0x00000016 System.Void PopcornMaker::Update()
extern void PopcornMaker_Update_m34E19E08B92AFDF0A860315883A6F8356A6604DF (void);
// 0x00000017 System.Void PopcornMaker::SpawnCube()
extern void PopcornMaker_SpawnCube_m97176F316BEEC3928E064551570D289F4E6AD5B1 (void);
// 0x00000018 System.Void PopcornMaker::RemoveCube()
extern void PopcornMaker_RemoveCube_m88723E29D06F39DE9A5282F858CE8384955B6500 (void);
// 0x00000019 System.Collections.IEnumerator PopcornMaker::KillCube(UnityEngine.GameObject)
extern void PopcornMaker_KillCube_mFF3E79FF001A7938572FEB0AF3F2AD13E3274DB0 (void);
// 0x0000001A System.Void PopcornMaker::.ctor()
extern void PopcornMaker__ctor_mCB78A5D0A078C6A6B01ECD49EECE2F47074CE1B9 (void);
// 0x0000001B System.Void PopcornMaker/<KillCube>d__13::.ctor(System.Int32)
extern void U3CKillCubeU3Ed__13__ctor_m32FF101BB9033EA517AC75D32E47F770E6022BE7 (void);
// 0x0000001C System.Void PopcornMaker/<KillCube>d__13::System.IDisposable.Dispose()
extern void U3CKillCubeU3Ed__13_System_IDisposable_Dispose_m0ADEAC43BAC26008EF5CEAFEAE12C680CF95D779 (void);
// 0x0000001D System.Boolean PopcornMaker/<KillCube>d__13::MoveNext()
extern void U3CKillCubeU3Ed__13_MoveNext_m953402D1A666891F18C1E2ED98629B67CAA3EAD3 (void);
// 0x0000001E System.Object PopcornMaker/<KillCube>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CKillCubeU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m32F32B14F389D4FA3A7AB75FAB6E63BBA7294109 (void);
// 0x0000001F System.Void PopcornMaker/<KillCube>d__13::System.Collections.IEnumerator.Reset()
extern void U3CKillCubeU3Ed__13_System_Collections_IEnumerator_Reset_m513F3C93CF55EE747191E0D3080BF21635DA6F6B (void);
// 0x00000020 System.Object PopcornMaker/<KillCube>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CKillCubeU3Ed__13_System_Collections_IEnumerator_get_Current_m2AD56A73B90513F00685D342C092ABA139F6A3C7 (void);
// 0x00000021 System.Void rotateIt::Start()
extern void rotateIt_Start_m28324FF9A850DBC784B6B72D8AF7E63DDC09F630 (void);
// 0x00000022 System.Void rotateIt::Update()
extern void rotateIt_Update_m62A849DB4472E43286979908AEB73800FED50A91 (void);
// 0x00000023 System.Void rotateIt::.ctor()
extern void rotateIt__ctor_mB7ECC6598089B6860BB870CC9755629E8E55AF72 (void);
static Il2CppMethodPointer s_methodPointers[35] = 
{
	FreeCam_Update_m0E525803336780E25890CE2E36C59D00FF0174BE,
	FreeCam_OnDisable_m383EA93DC78C8D2ABB533AD1A52728B1C8C0F583,
	FreeCam_StartLooking_mA80F357671A73F3337CD527CCEBDF84357F7798D,
	FreeCam_StopLooking_mBCED5313D885DF0DAC563FEA56E8D80D62CFF0A4,
	FreeCam__ctor_m718FB70858AD27D4118F107BADDEA789E6FA5AC7,
	FreeCamera_Awake_m3B544F81CD5A7CC7B1110954697069EDCAF19050,
	FreeCamera_OnValidate_m1C0B74AF3C24422FAF1ABB2164496634FF5A40A2,
	FreeCamera_CaptureInput_m2A98EA3929399EEBC609E4C7B793BBA6153A5DCC,
	FreeCamera_ReleaseInput_m3323C533F089391E8E97F5506213B0D37BD421A6,
	FreeCamera_OnApplicationFocus_m34A575C443ABB9F2ED9C351D9AA4F3D3B45723E2,
	FreeCamera_Update_m3B78A297B74E8DBB800B383700970C548B796281,
	FreeCamera__ctor_m5D39D27AEC0DBFCC7FDE8470605D6E0A7A1BA357,
	SuperCamera_Start_m8CFB1B49FDB2DA4118EA56A560E102BA5CEBF3B8,
	SuperCamera_Update_m11D2D6E9A26595BDE649F865FDB733AED6F3EA15,
	SuperCamera__ctor_m2D5DF21B9F75016763866ED241A0481B34D86863,
	UIButton_ShowNext_Start_m1C46EEA00D55364FBC3BD560FB3E8B3F3D11666D,
	UIButton_ShowNext_SelectNextGameObject_m06ACE4636D8DB6650B2DAED0D3335C3162F3280F,
	UIButton_ShowNext_SelectPreviousGameObject_mD21B08B5EEC5049635EEE7A85C9FC40CD8C0CE88,
	UIButton_ShowNext_SelectGameObject_mC299122AECE4C47905FBF0D2179FBC4A84E7300B,
	UIButton_ShowNext__ctor_mE73382473DB9B72F22C0375EDF19BFD2B3529C94,
	PopcornMaker_Start_m8A37DAF3E3A04C6BB318212EFC6CD35F899579C7,
	PopcornMaker_Update_m34E19E08B92AFDF0A860315883A6F8356A6604DF,
	PopcornMaker_SpawnCube_m97176F316BEEC3928E064551570D289F4E6AD5B1,
	PopcornMaker_RemoveCube_m88723E29D06F39DE9A5282F858CE8384955B6500,
	PopcornMaker_KillCube_mFF3E79FF001A7938572FEB0AF3F2AD13E3274DB0,
	PopcornMaker__ctor_mCB78A5D0A078C6A6B01ECD49EECE2F47074CE1B9,
	U3CKillCubeU3Ed__13__ctor_m32FF101BB9033EA517AC75D32E47F770E6022BE7,
	U3CKillCubeU3Ed__13_System_IDisposable_Dispose_m0ADEAC43BAC26008EF5CEAFEAE12C680CF95D779,
	U3CKillCubeU3Ed__13_MoveNext_m953402D1A666891F18C1E2ED98629B67CAA3EAD3,
	U3CKillCubeU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m32F32B14F389D4FA3A7AB75FAB6E63BBA7294109,
	U3CKillCubeU3Ed__13_System_Collections_IEnumerator_Reset_m513F3C93CF55EE747191E0D3080BF21635DA6F6B,
	U3CKillCubeU3Ed__13_System_Collections_IEnumerator_get_Current_m2AD56A73B90513F00685D342C092ABA139F6A3C7,
	rotateIt_Start_m28324FF9A850DBC784B6B72D8AF7E63DDC09F630,
	rotateIt_Update_m62A849DB4472E43286979908AEB73800FED50A91,
	rotateIt__ctor_mB7ECC6598089B6860BB870CC9755629E8E55AF72,
};
static const int32_t s_InvokerIndices[35] = 
{
	2076,
	2076,
	2076,
	2076,
	2076,
	2076,
	2076,
	2076,
	2076,
	1754,
	2076,
	2076,
	2076,
	2076,
	2076,
	2076,
	2076,
	2076,
	1715,
	2076,
	2076,
	2076,
	2076,
	2076,
	1345,
	2076,
	1715,
	2076,
	2052,
	2023,
	2076,
	2023,
	2076,
	2076,
	2076,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	35,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
